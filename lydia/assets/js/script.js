//------------------------------- Hamburger Dropdown---------------------------


/* Window Scrolling Effect
*/
$(document).ready(function(){

// Hamburger dropdown mobile view

$("#hamburger").click(function(){
	$("#menu").toggle();
	$(this).children('i').toggleClass('fa-bars');
	$(this).children('i').toggleClass('fa-times');

});

$(".list1").click(function(){ $(".dropdown1").toggle(); });

$(".list2").click(function(){
	$(".dropdown2").toggle();
	if($("#menu").css("height") == "85vh") {
		$(this).css("overflow","auto");
	}
});

$(".list3").click(function(){
	$(".dropdown3").toggle();
	if($("#menu").css("height") == "85vh") {
		$(this).css("overflow","auto");
	}
});

// Navigation Scrolling
var b = $('.progress-list').offset().top + $('.progress-list').height();
$(this).scroll(function(){
	var x = $(this).scrollTop();
	$(".animate-progress:hidden").css("display","none");
	if(x>=10){
		$("header").css("background-color","#000");
	}
	else if(x<10){
		$("header").css("background-color","transparent");
	}
	var a = $(window).scrollTop() + $(window).height();
	if(a > b){
		$(".animate-progress").css("animation","animation-p 0.5s linear");
		$(".animate-progress").css("animation-fill-mode","both");
	}
});

// Multiple Slider (Owl Carousel)
$('.owl-carousel').owlCarousel({
	loop:true,
	margin:10,
	nav:true,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:2
		},
		1000:{
			items:3
		}
	}
});

// Document Ready Ends Here
});

/*-----------------------------------Multiple Image Slider-------------------------*/














